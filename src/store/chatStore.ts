import { create } from 'zustand'
import { ChatLog, Models } from '../types/chatTypes'
import { ModalProps } from '../components/modal/Modal'

type Store = {
  chatLogHistory: ChatLog[]
  selectedChatLog: ChatLog | null
  systemModels: []
  selectedModelId: Models
  modalProps: ModalProps | null
  showSideMenu: boolean
  setSelectedModelId: (selectedModeId: Models) => void
  setSelectedChatLog: (selectedChatLog: ChatLog | null) => void
  addOrEditChatLog: (chatLog: ChatLog) => void
  deleteChatLog: (chatLogId: number) => void
  setModalProps: (showModal: ModalProps | null) => void
  toggleSideMenu: () => void
}

const useChatStore = create<Store>()((set) => ({
  chatLogHistory: [],
  selectedChatLog: null,
  systemModels: [],
  selectedModelId: 'gpt-3.5-turbo',
  modalProps: null,
  showSideMenu: true,
  setSelectedModelId: (selectedModelId: Models) => set((state) => ({ ...state, selectedModelId })),
  setSelectedChatLog: (selectedChatLog: ChatLog | null) => set((state) => ({ ...state, selectedChatLog })),
  addOrEditChatLog: (chatLog: ChatLog) =>
    set((state) => ({ ...state, chatLogHistory: addOrEditChat(state.chatLogHistory, chatLog) })),
  deleteChatLog: (chatLogId: number) =>
    set((state) => ({ ...state, chatLogHistory: deleteChat(state.chatLogHistory, chatLogId) })),
  setModalProps: (modalProps: ModalProps | null) => set((state) => ({ ...state, modalProps })),
  toggleSideMenu: () => set((state) => ({ ...state, showSideMenu: !state.showSideMenu })),

}))

export default useChatStore

const addOrEditChat = (state: ChatLog[], chatLog: ChatLog) => {
  const index = state.findIndex((item) => item.id === chatLog.id)
  const stateCopy = [...state]
  index !== -1 ? (stateCopy[index] = chatLog) : stateCopy.push(chatLog)
  return stateCopy
}

const deleteChat = (chatLogsInState: ChatLog[], chatLogId: number) => {
  return chatLogsInState.filter((item) => item.id !== chatLogId)
}

// import { create } from 'zustand'
// import { ChatLog, Models } from '../types/chatTypes'
// import { persist } from 'zustand/middleware'

// type Store = {
//   chatLogHistory: ChatLog[]
//   selectedChatLog: ChatLog
//   systemModels: []
//   selectedModelId: Models
//   setSelectedModelId: (selectedModeId: Models) => void
//   setSelectedChatLog: (selectedChatLog: ChatLog | null) => void
//   mutateChatLogHistory: (chatLog: ChatLog) => void
// }

// const chatStore = (set: any) => ({
//   chatLogHistory: [],
//   selectedChatLog: null,
//   systemModels: [],
//   selectedModelId: 'gpt-3.5-turbo',
//   setSelectedModelId: (selectedModelId: Models) => set((state: Store) => ({ ...state, selectedModelId })),
//   setSelectedChatLog: (selectedChatLog: ChatLog | null) =>
//     set((state: Store) => ({ ...state, selectedChatLog })),
//   mutateChatLogHistory: (chatLog: ChatLog) =>
//     set((state: Store) => ({ ...state, chatLogHistory: mutateChatLog(state.chatLogHistory, chatLog) })),
// })

// // Use the persist middleware to create the store with persistence
// const useChatStore = create(
//   persist(chatStore, {
//     name: 'chat-store', // A unique name for the storage key
//     getStorage: () => localStorage, // Use localStorage for persistence
//   })
// )

// const mutateChatLog = (state: ChatLog[], chatLog: ChatLog) => {
//   const index = state.findIndex((item) => item.id === chatLog.id)
//   const stateCopy = [...state]
//   index !== -1 ? (stateCopy[index] = chatLog) : stateCopy.push(chatLog)
//   return stateCopy
// }

// export default useChatStore
