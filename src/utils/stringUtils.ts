import { encode } from 'gpt-tokenizer'

export const countTokens = (text: string) => {
  const encodedText = encode(text)
  return encodedText.length
}
