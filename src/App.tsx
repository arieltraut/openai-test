import './App.css'
import Chat from './components/chat/Chat'
import Header from './components/header/Header'
import { Modal } from './components/modal/Modal'
import SideMenu from './components/side-menu/SideMenu'
import useChatStore from './store/chatStore'

function App() {
  const { modalProps } = useChatStore()
  return (
    <div className="App min-h-screen bg-renaiss-bg">
      <Header />
      <main className='h-[calc(100vh-95px)]'>
        <div className="p-[32px] flex justify-items-center justify-center gap-6 mx-auto h-full">
          <SideMenu />
          <Chat />
        </div>
      </main>
      {modalProps && <Modal {...modalProps} />}
    </div>
  )
}

export default App
