import { openai } from './config'
import { ChatCompletionReq } from '../types/chatTypes'

export const openaiService = {
  chatCompletion: (request: ChatCompletionReq) => {
    const { model = 'gpt-3.5-turbo', messages } = request
    return openai.createChatCompletion({
      model,
      messages,
    })
  },
  createImage: (prompt: string) => {
    return openai.createImage({
        prompt,
        n: 1,
        size: "1024x1024",
      });
      //image_url = response.data.data[0].url;
  },
  
  fetchModels: () => openai.listModels()
}


