import { FC } from 'react'
import { formatTime } from '../../../utils/dateUtils'
import { LocalMessage } from '../../../types/chatTypes'
import { formatCodeBlocks } from './CodeblockWrapper'

interface ChatBubbleProps extends LocalMessage {
  key: number | string
}

const ChatBubble: FC<ChatBubbleProps> = ({ name = 'OdamaChat', content, created, model, role }) => {
  const headerTextColor = name === 'OdamaChat' ? 'text-renaiss-primary' : 'text-renaiss-secondary'

  return (
    <div className={'bg-white shadow-xl flex-grow-1 flex-shrink-0 rounded-[10px] w-full px-6 min-h-[128px] mb-6'}>
      <header className="border-b border-renaiss-border/60 min-h-[64px] flex items-center">
        <h2 className={`${headerTextColor} text-lg font-semibold`}>{name}</h2>
        <span className="text-[#94A3B8] text-sm px-4">{formatTime(new Date(created!))}</span>
      </header>
      <div className="text-renaiss-paragraph py-6 text-[1.2rem] leading-8 whitespace-pre-line w-full">
        {model === 'DALL·E (Create image)' && role === 'assistant' ? (
          <img className="w-[60%] block mx-auto rounded-xl" src={content} alt="" />
        ) : (
          <>{formatCodeBlocks(content)}</>
        )}
      </div>
    </div>
  )
}

export default ChatBubble

export const ChatBubbleSkeleton = () => (
  <div className={'bg-white shadow-xl flex-grow-0 flex-shrink-0 rounded-[10px] w-full px-6'}>
    <header className="min-h-[64px] flex items-center">
      <h2 className={'text-renaiss-primary text-lg font-semibold'}>OdamaChat ...</h2>
    </header>
  </div>
)
