import React from 'react'

interface CodeBlockProps {
  language: string
  code: string
}

export const formatCodeBlocks = (text: string) => {
  const codeBlockRegex = /```(\w+)\n([\s\S]+?)\n```/g

  const parts = text?.split(codeBlockRegex)
  console.log(parts)
  return parts?.map((part, index) => {
    if (index % 3 === 2) {
      const language = parts[index - 1]
      return <CodeBlock key={index} language={language} code={part} />
    } else if (index % 3 !== 1) {
      return part
    }
  })
}

const CodeBlock: React.FC<CodeBlockProps> = ({ language, code }) => {
  return (
    <pre className='max-w-[80%] bg-gray-200 rounded-xl p-6'>
      <code lang={language} className={''}>
        {code}
      </code>
    </pre>
  )
}
