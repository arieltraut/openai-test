import { useMemo, useRef, useState } from 'react'
import SectionHeader from '../section-header/SectionHeader'
import TextInput from '../text-input/TextInput'
import { NewChatIcon } from './assets/NewChatIcon'
import ChatBubble, { ChatBubbleSkeleton } from './components/ChatBubble'
import { ChatLog, LocalMessage } from '../../types/chatTypes'
import useOpenaiService from '../../hooks/useOpenaiService'
import useScrollOnEvent from '../../hooks/useScrollOnEvent'
import useChatStore from '../../store/chatStore'
import { inputtedTextValidation } from '../../business/businessLogic'

const Chat = () => {
  const [userInput, setUserInput] = useState<string>('')
  const userInputRef = useRef<HTMLInputElement>(null)
  const { selectedChatLog, selectedModelId, setSelectedChatLog, addOrEditChatLog } = useChatStore()
  const { callService, isLoading } = useOpenaiService()
  const { scrollContainerRef } = useScrollOnEvent(selectedChatLog?.messages?.length)
  const { maxTokensSurpased, tokensInputted } = useMemo(() => inputtedTextValidation(userInput), [userInput])

  const handleSubmit = async () => {
    if (maxTokensSurpased) return
    setUserInput('')
    const userMessage: LocalMessage = {
      role: 'user',
      content: userInput,
      model: selectedModelId,
      name: 'Ana Clara',
      created: Date.now(),
    }
    const tempChatLog: ChatLog = {
      id: selectedChatLog?.id ?? undefined,
      chatName: selectedChatLog?.chatName ?? userInput,
      created: userMessage.created!,
      messages: selectedChatLog ? [...selectedChatLog?.messages, userMessage] : [userMessage],
    }
    setSelectedChatLog(tempChatLog)
    const responseMessage: LocalMessage = await callService({ model: selectedModelId, message: userInput })
    if (responseMessage) {
      const withServerResponse: ChatLog = {
        ...tempChatLog,
        id: tempChatLog.id || responseMessage.created,
        messages: [...tempChatLog.messages, responseMessage],
      }
      addOrEditChatLog(withServerResponse)
      setSelectedChatLog(withServerResponse)
    } else {
      const withLastMsgRemoved = tempChatLog.messages.slice(0, -1)
      setSelectedChatLog({ ...tempChatLog, messages: withLastMsgRemoved })
    }
  }

  const startNewChat = () => {
    setUserInput('')
    setSelectedChatLog(null)
    userInputRef?.current?.focus()
  }

  return (
    <section className={'border-renaiss-border/60 border rounded-[10px] flex flex-col h-full w-full'}>
      <SectionHeader button={{ children: <NewChatIcon />, onClick: startNewChat }}>OdamaChat</SectionHeader>
      <div
        className="flex grow px-6 pt-9 h-full overflow-y-auto items-start flex-col"
        ref={scrollContainerRef}
      >
        {selectedChatLog?.messages?.map((message) => {
          return <ChatBubble {...message} key={message.created!} />
        })}
        {isLoading && <ChatBubbleSkeleton />}
      </div>
      <footer className="bg-white min-h-[99px] border-t border-renaiss-border/60 rounded-b-[10px] flex items-center flex-col px-6 pt-8">
        <TextInput
          ref={userInputRef}
          onClick={handleSubmit}
          disabled={isLoading}
          value={userInput}
          onChange={(e) => setUserInput(e.target.value)}
          error={maxTokensSurpased}
        />
        <div className="min-h-[1.5rem] p-1 text-xs w-full">
          {!maxTokensSurpased && tokensInputted > 0 && (
            <span className="text-gray-400">Cantidad de tokens ingresados: {tokensInputted}</span>
          )}
          {maxTokensSurpased && (
            <span className="text-red-600">Superada la cantidad permitida de carácteres</span>
          )}
        </div>
      </footer>
    </section>
  )
}

export default Chat
