import { ButtonHTMLAttributes, FC } from 'react'

interface SectionHeaderProps {
  children: string
  button?: ButtonHTMLAttributes<HTMLButtonElement>
}

const SectionHeader: FC<SectionHeaderProps> = ({ children, button }) => {
  return (
    <header
      className={
        'bg-white rounded-t-[10px] min-h-[64px] flex items-center justify-between px-6 border-b border-renaiss-border/60'
      }
    >
      <h2 className="text-[#1E293B] text-lg font-semibold">{children}</h2>
      {button && <button className='bg-renaiss-primary rounded-md' {...button} />}
    </header>
  )
}

export default SectionHeader
