import { forwardRef } from 'react'
import inputIcons from './assets/input-icons.png'

interface TextInputProps extends React.InputHTMLAttributes<HTMLInputElement> {
  label?: string
  onClick: () => void
  error?: boolean
}

const TextInput = forwardRef<HTMLInputElement, TextInputProps>((props, ref) => {
  const { label, onClick, error, placeholder = 'Insertar Prompt', ...rest } = props

  const handleKeyPress = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === 'Enter') {
      onClick?.()
    }
  }

  return (
    <div className="relative w-full">
      <input
        {...rest}
        ref={ref}
        onKeyDown={handleKeyPress}
        placeholder={placeholder}
        className={`bg-white ${
          error ? 'border-red-600' : 'border-[#CBD5E1]'
        } text-renaiss-paragraph w-full px-4 py-3 m-0 rounded-lg border focus:${
          error ? 'border-red-600' : 'border-renaiss-primary'
        } focus:outline-none`}
      />
      <button disabled={rest.disabled} onClick={onClick} className="absolute right-[11px] top-[3px]">
        <img src={inputIcons} className="h-[44px]"></img>
      </button>
    </div>
  )
})

export default TextInput
