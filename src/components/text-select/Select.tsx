import React, { FC, useEffect, useState } from 'react'
import inputSend from './assets/input-send.png'

interface SelectComponentProps {
  options: string[]
  onChange: (selectedOption: string) => void
  initialValue: string
}

const SelectComponent: FC<SelectComponentProps> = ({ options, onChange, initialValue }) => {
  const [selectedValue, setSelectedValue] = useState<string | undefined>(initialValue)

  const handleSelectChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    const selectedOption = event.target.value
    setSelectedValue(selectedOption)
    onChange(selectedOption)
  }

  useEffect(() => {
    setSelectedValue(initialValue)
  }, [initialValue])

  return (
    <div className="relative w-full">
      <select
        className="bg-white border-[#CBD5E1] text-renaiss-paragraph w-full px-4 py-3 m-0 rounded-lg border focus:border-renaiss-primary focus:outline-none appearance-none"
        onChange={handleSelectChange}
        value={selectedValue}
      >
        {options?.map((option) => (
          <option key={option} value={option}>
            {option}
          </option>
        ))}
      </select>
      <button disabled className="absolute right-[11px] top-[3px]">
        <img src={inputSend} className="h-[44px]"></img>
      </button>
    </div>
  )
}

export default SelectComponent
