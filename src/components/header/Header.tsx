import { FC, ReactNode, useState } from 'react'
import { AsideToggle, AsideToggleFilled } from './assets/AsideToggle'
import { SettingsIcon } from './assets/SettingsIcon'
import { BackIcon } from './assets/BackIcon'
import useChatStore from '../../store/chatStore'
import System from '../side-menu/components/System'
import History from '../side-menu/components/History'

const Header = () => {
  const [showMobileMenu, setShowMobibleMenu] = useState(false)
  const { toggleSideMenu } = useChatStore()

  const toggle = () => {
    console.log(window.innerWidth)
    if (window.innerWidth < 1280) {
      setShowMobibleMenu((prevState) => !prevState)
    } else {
      toggleSideMenu()
    }
  }

  return (
    <>
      <header className={'bg-renaiss-primary'}>
        <nav>
          <ul className={'min-h-[95px] mx-[36px] flex items-center gap-[10px] [&>*:nth-child(2)]:mr-auto'}>
            <NavButton>
              <BackIcon />
            </NavButton>
            <NavButton onClick={toggle}>
              <AsideToggle />
            </NavButton>
            <NavButton>
              <SettingsIcon />
            </NavButton>
          </ul>
        </nav>
      </header>
      {showMobileMenu && <MobileMenu toggleMobileSideMenu={toggle}/>}
    </>
  )
}

export default Header

const NavButton = ({ children, onClick }: { children: ReactNode; onClick?: () => void }) => (
  <li className={''}>
    <button onClick={onClick}>
      <span>{children}</span>
    </button>
  </li>
)

const MobileMenu: FC<{toggleMobileSideMenu: () => void}> = ({toggleMobileSideMenu}) => (
  <div className="w-[100%] bg-renaiss-primary flex-col gap-6 shrink-0 absolute top-20 bottom-0 flex z-10 px-4 pb-4 md:hidden">
    <System />
    <History toggleMobileSideMenu={toggleMobileSideMenu}/>
  </div>
)
