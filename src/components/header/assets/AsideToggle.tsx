export const AsideToggleFilled = () => {
  return (
    <svg width="45" height="45" viewBox="0 0 45 45" fill="none" xmlns="http://www.w3.org/2000/svg">
      <rect width="45" height="44.9999" rx="4" fill="white" />
      <path
        d="M22.5 13.125C26.0937 13.125 28.4271 13.6354 29.8646 15.0209C31.3021 16.4063 31.875 18.8021 31.875 22.5C31.875 26.0104 31.3958 28.3125 30.0729 29.7917C28.75 31.2708 26.2812 31.875 22.5 31.875C18.7187 31.875 16.25 31.2916 14.8542 29.6771C13.5937 28.2292 13.125 25.9479 13.125 22.5C13.125 18.8542 13.6458 16.5104 15.0729 15.0834C16.5 13.6563 18.8542 13.125 22.5 13.125Z"
        stroke="#F97316"
        strokeWidth="1.5625"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M24.5833 31.6146V13.3854"
        stroke="#F97316"
        strokeWidth="1.5625"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M20.4167 18.3333H17.2917"
        stroke="#F97316"
        strokeWidth="1.5625"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M20.4167 22.5H17.2917"
        stroke="#F97316"
        strokeWidth="1.5625"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}

export const AsideToggle = () => {
  return (
    <svg width="45" height="45" viewBox="0 0 45 45" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M22.5 13.125C26.0937 13.125 28.4271 13.6354 29.8646 15.0209C31.3021 16.4063 31.875 18.8021 31.875 22.5C31.875 26.0104 31.3958 28.3125 30.0729 29.7917C28.75 31.2708 26.2812 31.875 22.5 31.875C18.7187 31.875 16.25 31.2916 14.8542 29.6771C13.5937 28.2292 13.125 25.9479 13.125 22.5C13.125 18.8542 13.6458 16.5104 15.0729 15.0834C16.5 13.6563 18.8542 13.125 22.5 13.125Z"
        stroke="white"
        strokeWidth="1.5625"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M24.5833 31.6146V13.3854"
        stroke="white"
        strokeWidth="1.5625"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M20.4166 18.3333H17.2916"
        stroke="white"
        strokeWidth="1.5625"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M20.4166 22.5H17.2916"
        stroke="white"
        strokeWidth="1.5625"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <rect x="0.75" y="0.75" width="43.5" height="43.4999" rx="3.25" stroke="white" strokeWidth="1.5" />
    </svg>
  )
}
