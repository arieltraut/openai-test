import { useEffect } from 'react'
import useChatStore from '../../store/chatStore'
import History from './components/History'
import System from './components/System'

const SideMenu = () => {
  const { showSideMenu, toggleSideMenu } = useChatStore()

  return (
    <>
      {showSideMenu && (
        <div className="w-[564px] flex-col gap-6 shrink-0 hidden left-[-100%] xl:flex h-full">
          <System />
          <History />
        </div>
      )}
    </>
  )
}

export default SideMenu
