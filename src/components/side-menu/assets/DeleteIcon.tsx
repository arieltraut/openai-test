export const DeleteIcon = () => {
  return (
    <svg width="40" height="40" viewBox="490 15 18 31" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M504.065 28.0325C504.065 37.0801 504.065 37.9026 499.13 37.9026C494.195 37.9026 494.195 37.0801 494.195 28.0325"
        stroke="#FDBA74"
        strokeWidth="1.54221"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M492.55 25.5649C496.936 25.5649 501.323 25.5649 505.71 25.5649"
        stroke="#FDBA74"
        strokeWidth="1.54221"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M497.485 23.0974C500.775 23.0974 501.186 23.0974 501.186 23.0974"
        stroke="#FDBA74"
        strokeWidth="1.54221"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}
