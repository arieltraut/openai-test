export const SearchIcon = () => {
  return (
    <svg width="40" height="50" viewBox="20 5 40 50" fill="none" xmlns="http://www.w3.org/2000/svg">
      <rect x="20" y="13" width="35" height="35" rx="17.5" fill="#FDBA74" />
      <path
        d="M39.9305 32.9306L44.7917 37.7917"
        stroke="white"
        strokeWidth="1.21528"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M36.8796 34.5509C39.0118 34.5509 41.5509 32.0118 41.5509 28.8796C41.5509 25.7475 39.0118 23.2083 35.8796 23.2083C32.7474 23.2083 30.2083 25.7475 30.2083 28.8796C30.2083 32.0118 32.7474 34.5509 35.8796 34.5509Z"
        stroke="white"
        strokeWidth="1.21528"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}
