export const TimeIcon = () => {
  return (
    <svg className="mr-2 h-4 w-4" width="20" height="20" viewBox="49 25 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M56.215 25.8388C57.849 25.8388 59.4162 26.4879 60.5716 27.6434C61.7271 28.7988 62.3762 30.3659 62.3762 32"
        stroke="#94A3B8"
        strokeWidth="1.02687"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <circle
        cx="56.215"
        cy="32"
        r="6.16123"
        stroke="#94A3B8"
        strokeWidth="1.02687"
        strokeLinecap="round"
        strokeDasharray="0.07 2.4"
      />
      <path
        d="M55.8727 29.2617V32.6846H59.2956"
        stroke="#94A3B8"
        strokeWidth="1.02687"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}
