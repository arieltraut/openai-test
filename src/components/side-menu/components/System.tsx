import { useEffect, useState } from 'react'
import SelectComponent from '../../text-select/Select'
import useChatStore from '../../../store/chatStore'
import { Models } from '../../../types/chatTypes'

const System = () => {
  const { setSelectedModelId, selectedModelId } = useChatStore()
  const models: Models[] = ['DALL·E (Create image)', 'gpt-3.5-turbo']

  const handleSelectChange = (selectedOption: string) => {
    setSelectedModelId(selectedOption as Models);
  };

  useEffect(() => {
    console.log({selectedModelId})
  }, [selectedModelId])

  return (
    <div
      className={
        'bg-white shadow-xl flex flex-col flex-grow-0 flex-shrink-0 rounded-[10px] w-full px-6 pb-10 min-h-[229px]'
      }
    >
      <header className="min-h-[64px] flex items-center">
        <h2 className={'text-renaiss-paragraph text-[22px] font-semibold'}>Sistema</h2>
      </header>
      <p className="text-[#64748B] grow max-w-[45ch]">
        {'Para conseguir una respuesta adecuada a tus necesidades, escribe un prompt para el sistema.'}
      </p>
      <footer>
        <SelectComponent options={models} onChange={handleSelectChange} initialValue={selectedModelId} />
      </footer>
    </div>
  )
}

export default System
