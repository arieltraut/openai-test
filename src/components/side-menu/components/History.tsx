import { FC } from 'react'
import useChatStore from '../../../store/chatStore'
import SectionHeader from '../../section-header/SectionHeader'
import HistoryItem from './HistoryItem'

const History: FC<{toggleMobileSideMenu?: () => void}> = ({toggleMobileSideMenu}) => {
  const { chatLogHistory } = useChatStore()

  return (
    <div
      className={
        'bg-white shadow-xl flex flex-col flex-grow flex-shrink-0 rounded-[10px] w-full h-[calc(100%-229px-24px);]'
      }
    >
      <SectionHeader>Historial de Búsquedas</SectionHeader>
      <ul className="p-6 h-full overflow-y-auto">
        {chatLogHistory.map((chatLog) => (
          <HistoryItem key={chatLog.id} {...chatLog} toggleMobileSideMenu={toggleMobileSideMenu} />
        ))}
      </ul>
    </div>
  )
}

export default History
