import { FC } from 'react'
import useChatStore from '../../../store/chatStore'
import { ChatLog } from '../../../types/chatTypes'
import { DeleteIcon } from '../assets/DeleteIcon'
import { SearchIcon } from '../assets/SearchIcon'
import { TimeIcon } from '../assets/TimeIcon'
import { getRelativeTime } from '../../../utils/dateUtils'

interface HistoryItemProps extends ChatLog {
  toggleMobileSideMenu?: () => void
}

const HistoryItem: FC<HistoryItemProps> = ({ toggleMobileSideMenu, ...chatLog }) => {
  const { selectedChatLog, setSelectedChatLog, setModalProps, deleteChatLog } = useChatStore()
  const isSelected: boolean = selectedChatLog?.id === chatLog.id

  const setAsSelected = () => {
    setSelectedChatLog(chatLog)
    if (window.innerWidth < 1280) toggleMobileSideMenu?.()
  }

  const showDeleteModal = () => {
    setModalProps({
      content: '¿Desea eliminar el chat?',
      title: 'Atencion',
      buttonProps: { label: 'Eliminar chat', onClick: () => deleteChatLog(chatLog.id!) },
    })
  }

  return (
    <li
      className={`${
        isSelected ? 'bg-[#FFEDD580]' : ''
      } min-h-[61px] px-6 mb-4 flex flex-col justify-center rounded`}
    >
      <div className="flex w-full">
        <button className="flex text-left grow items-center" onClick={setAsSelected}>
          <span className="mr-3">
            <SearchIcon />
          </span>
          <div className="flex flex-col">
            <span className="text-renaiss-paragraph">{chatLog.chatName}</span>
            <span className="flex items-center text-[#64748B] mr-auto">
              <TimeIcon /> {getRelativeTime(chatLog.created)}
            </span>
          </div>
        </button>
        {!isSelected && (
          <button onClick={showDeleteModal} className="">
            <DeleteIcon />
          </button>
        )}
      </div>
    </li>
  )
}

export default HistoryItem
