import { useEffect, useRef, useState } from 'react'

type useSrollOnEventResponseType = {
    scrollContainerRef: React.RefObject<HTMLDivElement>
}

const useSrollOnEvent = (observableEl: any) => {
  const scrollContainerRef = useRef<HTMLDivElement>(null)

  useEffect(() => {
    scrollToBottom()
  }, [observableEl])

  const scrollToBottom = () => {
    const scrollContainer = scrollContainerRef.current
    if (scrollContainer) {
      scrollContainer.scrollTop = scrollContainer.scrollHeight
    }
  }

  return { scrollContainerRef } as useSrollOnEventResponseType
}

export default useSrollOnEvent
