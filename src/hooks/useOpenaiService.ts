import { useCallback, useEffect, useState } from 'react'
import {
  ChatCompletionErrorRes,
  ChatCompletionReq,
  LocalMessage,
  Requests,
} from '../types/chatTypes'
import { openaiService } from '../services/openaiService'

type useOpenaiServiceResponseType = {
  models: any
  //createChatCompletion: (messages: ChatCompletionReq) => Promise<AdaptedChatCompletionRes>
  callService: (request: Requests) => Promise<LocalMessage>
  httpError: ChatCompletionErrorRes
  isLoading: boolean
}

type ServiceCallFunction = (...args: any[]) => Promise<any>

const useOpenaiService = (autoFetch?: boolean) => {
  const [httpError, setHttpError] = useState<{ message: string } | null>(null)
  const [models, setModels] = useState(null)
  const [isLoading, setIsLoading] = useState(false)

  useEffect(() => {
    if (!autoFetch) return
    ;(async () => {
      const models = await listModels()
      if (models) setModels(models)
    })()
  }, [])

  const makeRequest = async (serviceCall: ServiceCallFunction, ...args: any[]): Promise<any> => {
    setIsLoading(true)
    try {
      const response = await serviceCall(...args)
      return response
    } catch (error) {
      console.error(error)
      setHttpError(error as any)
    } finally {
      setIsLoading(false)
    }
  }

  const createChatCompletion = useCallback(async (chatCompletionReq: Requests): Promise<LocalMessage> => {
    const requestObj: ChatCompletionReq = {
      messages: [{ role: 'user', content: chatCompletionReq.message }],
      model: chatCompletionReq.model,
    }
    const response = await makeRequest(openaiService.chatCompletion, requestObj)
    console.log(response?.data?.choices[0]?.message)

    const localMessage: LocalMessage = {
      model: 'gpt-3.5-turbo',
      ...response?.data?.choices[0]?.message, //{role, content}
      created: response?.data?.created //ver llega fecha vieja      
    }
    return localMessage
  }, [])

  const listModels = async (): Promise<any> => {
    const response = await makeRequest(openaiService.fetchModels)
    const modelIds = response?.data?.data?.map((model: any) => model.id)
    return modelIds
  }

  const createImage = async (request: string): Promise<LocalMessage> => {
    const response = await makeRequest(openaiService.createImage, request)
    const localMessage: LocalMessage = {
      model: 'DALL·E (Create image)',
      content: response.data.data[0].url,
      role: 'assistant',
      created: response.data.created
    }
    return localMessage
  }

  const callService = async (request: Requests): Promise<LocalMessage> => {
    console.log(request.model)
    return request.model === 'gpt-3.5-turbo'
      ? createChatCompletion(request)
      : createImage(request.message)
  }

  return { models, callService, httpError, isLoading } as useOpenaiServiceResponseType
}

export default useOpenaiService
