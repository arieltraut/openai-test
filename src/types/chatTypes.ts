export interface ChatLog {
  id: number | undefined
  chatName: string
  created: number
  messages: LocalMessage[]
}

export interface LocalMessage extends Message {
  created?: number
  model: Models
}

type Role = 'system' | 'user' | 'assistant' | 'function'

export type Models = 'gpt-3.5-turbo' | 'DALL·E (Create image)'

export interface Requests {
  model: Models
  message: string
}






// Service interfaces
export interface Message {
  role: Role
  name?: string
  content: string
}

export interface ChatCompletionReq {
  model?: string //ver otros o string text-davinci-003
  messages: Message[]
}

export interface ChatCompletionRes {
  id: string
  object: string
  created: number
  choices: {
    index: number
    message: Message
    finish_reason: string
  }[]
  usage: {
    prompt_tokens: number
    completion_tokens: number
    total_tokens: number
  }
}

export interface AdaptedChatCompletionRes {
  responseId: string
  responseMessage: LocalMessage
}

export interface ChatCompletionErrorRes {
  code: any
  type: string
  message: string
}
