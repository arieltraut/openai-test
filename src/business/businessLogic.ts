import { countTokens } from '../utils/stringUtils'

const TOTAL_TOKENS_ALLOWED = 1000

export const inputtedTextValidation = (inputText: string): { tokensInputted: number; maxTokensSurpased: boolean } => {
  const tokensInputted: number = countTokens(inputText)
  return {
    tokensInputted,
    maxTokensSurpased: tokensInputted > TOTAL_TOKENS_ALLOWED,
  }
}