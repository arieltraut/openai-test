/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  theme: {
    extend: {
      colors: {
        renaiss: {
          dark: '#1c2026',
          light: '#31e7f7',
          primary: '#F97316',
          secondary: '#10B981',
          bg: '#F8FAFC',
          border: '#CCCCCC8C',
          paragraph: '#1E293B'
        },
      },
      boxShadow: {
        'xl': '0 0 6px 0 rgba(46, 58, 90, 0.09)',
      },
    },
    // fontFamily: {
    //   sans: ['var(--font-merri)'],
    //   mono: ['var(--font-roboto-mono)']
    // }
  },
  plugins: [],
}
